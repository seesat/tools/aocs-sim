%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   
%   SEESAT AOCS Simulator
%
%   ss_aocs_test_kinematic_ode.m
%
%   Test script to visualize quaternion rotation
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

box=[[-1; 5; -1],...
     [1; 5; -1],...
     [3; -5; -1],...
     [-3; -5; -1],...
     [-1; 5; 1],...
     [1; 5; 1],...
     [3; -5; 1],...
     [-3; -5; 1]];
 
box_handle= drawbox(box);

[i1, i2, i3]= size(quat.Data(1,:,:));

for i=1:100
    pause(0.01);
    q= quat.Data(1,:,i);
    R= quat2rotm(q);
    box_rotated=R*box;
    drawbox(box_rotated);
end