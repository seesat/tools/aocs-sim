%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   
%   SEESAT AOCS Simulator
%
%   ss_aocs_env_postprocessing.m
%
%   Plot disturbances caused by environmental model for a visual inspection
%   of results 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Plot Acceleration through athmospheric drag
% plot disturbance accelerations in all three body-axes  
a_drag_fig= figure('Name','A_drag',...                  % setting of which tools are shown in the figure
                     'Toolbar','Figure',...
                     'Menubar','none',...
                     'Numbertitle','off');
plot(simout_a_drag)                                     % plotting the disturbance accelerations in all three bodyaxes
ylabel('SC acceleration through atmospheric drag')      % labeling the plot
legend('x-Direction','y-Direction','z-Direction')       % all plots below are coded similar
grid on
hold

%% Earth's Flatness
% plot disturbance accelerations in all three body-axes 
a_flatness_fig= figure('Name','A_flatness',...
                     'Toolbar','Figure',...
                     'Menubar','none',...
                     'Numbertitle','off');
plot(simout_a_flatness)
ylabel('SC acceleration through earths flatness')
legend('x-Direction','y-Direction','z-Direction')
grid on
hold

%% Solar pressure
% plot disturbance accelerations in all three body-axes 
a_sol_pres_fig= figure('Name','A_sol_pres',...
                     'Toolbar','Figure',...
                     'Menubar','none',...
                     'Numbertitle','off');
plot(simout_a_sol_pres)
ylabel ('SC acceleration through solar pressure')
legend('x-Direction','y-Direction','z-Direction')
grid on
hold

%% Sun Moon gravitational disturbance
% not yet implemented





