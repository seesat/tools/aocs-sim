%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   
%   SEESAT AOCS Simulator
%
%   ss_aocs_attitude_postprocessing.m
%
%   Display spacecraft rotation
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Create box to visualize cubsat
box=[[5; -1; -1],...
     [5; 1; -1],...
     [-5; 3; -1],...
     [-5; -3; -1],...
     [5; -1; 1],...
     [5; 1; 1],...
     [-5; 3; 1],...
     [-5; -3; 1]];
 
box_handle= drawbox(box);

%% Figure for euler angles
fig_euler= figure('Name','Euler Angles',...
                 'Numbertitle','off');
ax_euler= axes(fig_euler);

subplot(3,1,1)
plot_eulerx= plot(nan);
axis([0 1000 -180 180])
xlabel('Time [s]')
ylabel('Roll angle')
grid on

subplot(3,1,2)
plot_eulery= plot(nan);
axis([0 1000 -90 90])
xlabel('Time [s]')
ylabel('Pitch angle')
grid on

subplot(3,1,3)
plot_eulerz= plot(nan);
axis([0 1000 -180 180])
xlabel('Time [s]')
ylabel('Yaw angle')
grid on

autoArrangeFigures(1,2)
%% Draw
i=1;
time=1:999;
pause(0.1);
while i<1000 && ishandle(fig_euler) && ishandle(box_handle)
    pause(0.00001);
    % Visualize Rotation
    q= quat.Data(i,:);
    R= quat2rotm([q(4),q(1:3)]);
    box_rotated=R*box;
    updatebox(box_handle,box_rotated);
    
    % Calculate Euler angles
    [phi(i), theta(i), psi(i)]=quat2angle([q(4),q(1:3)],'XYZ');

    plot_eulerx.YData= phi(1:i)*180/pi;

    plot_eulery.YData= theta(1:i)*180/pi;
    plot_eulerz.YData= psi(1:i)*180/pi;
    i=i+1;
end