%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   
%   SEESAT AOCS Simulator
%
%   ss_aocs_orbit_postprocessing.m
%
%   Analyze and plot the orbit simulation results.
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Plot earth orbit
%scrsz = get(groot,'ScreenSize');
orbit_3d_fig= figure('Name','3D Orbit',...
                     'Toolbar','Figure',...
                     'Menubar','none',...
                     'Numbertitle','off');%,...
                     %'Position');,[scrsz(1),...
                                % scrsz(2),...
                                % scrsz(3),...
                                % scrsz(4)]);
                             
orbit_3d_axes= axes(orbit_3d_fig,...
                    'Projection','perspective');
rotate3d(orbit_3d_axes,'on')

load topo topo topomap1
[x,y,z] = ellipsoid(0,0,0,R_earth,R_earth,R_earth,50);
props.AmbientStrength = 0.1;
props.DiffuseStrength = 1;
props.SpecularColorReflectance = .5;
props.SpecularExponent = 20;
props.SpecularStrength = 1;
props.FaceColor = 'texture';
props.EdgeColor = 'none';
props.FaceLighting = 'phong';
props.Cdata = topo;
roothandles.earth_handle= surface(orbit_3d_axes,x,y,z,props);
view(orbit_3d_axes,3)
grid(orbit_3d_axes,'on')
hold(orbit_3d_axes,'on')
axis(orbit_3d_axes,'equal')

plot3(orbit_3d_axes,r_I.Data(:,1),r_I.Data(:,2),r_I.Data(:,3),'k')
title('Low Earth Orbit of SeeSat')
xlabel('[m]')
ylabel('[m]')
zlabel('[m]')
orbit_3d_fig.ToolBar='none';

%% Plot spacecraft ground track

groundtrack_fig= figure( 'Name','Ground Track',...
                        'Toolbar','none',...
                        'Menubar','none',...
                         'Numbertitle','off');%,...
%                         'Position',[scrsz(1),...
%                                     scrsz(2)+scrsz(4)/2,...
%                                     scrsz(3),...
%                                     scrsz(4)/2]);


groundtrack_axes= axes(groundtrack_fig,...
                       'XLim',[-180 180],'YLim',[-90 90], ...
                       'XTick',[-180 -120 -60 0 60 120 180], ...
                       'Ytick',[-90 -60 -30 0 30 60 90]);


topo= [topo(:,180:359), topo(:,1:180)];                  
contour(groundtrack_axes,-179:180,-89:90,topo,[0 0])
hold(groundtrack_axes,'on')
groundtrack_fig.ToolBar='none';

plot(groundtrack_axes,sc_longitude.Data,sc_latitude.Data,'g.')
title('Path of SeeSat in a Low Earth Orbit (LEO)')
xlabel('Longitude [�]')
ylabel('Latitude [�]')

% Plot kepler elements

kepler_fig= figure( 'Name','Kepler Elements',...
                    'Toolbar','none',...
                    'Menubar','none',...
                    'Numbertitle','off');%,...
%                     'Position',[scrsz(1),...
%                                 scrsz(2),...
%                                 scrsz(3),...
%                                 scrsz(4)]);


kepler_axes= axes(kepler_fig);
subplot(3,2,1)
plot(kepler.Data(:,1))
axis([0 inf 0.5*min(kepler.Data(:,1)) 1.3*max(kepler.Data(:,1))])
xlabel('Time [s]')
ylabel('Semi-Major Axis [m]')
grid on

subplot(3,2,2)
plot(kepler.Data(:,2))
axis([0 inf 0.5*min(kepler.Data(:,2)) 1.3*max(kepler.Data(:,2))])
xlabel('Time [s]')
ylabel('Eccentricity [-]')
grid on

subplot(3,2,3)
plot(180-kepler.Data(:,3)*180/pi)
axis([0 inf 0 1.3*max(kepler.Data(:,3))*180/pi])
xlabel('Time [s]')
ylabel('Inclination [�]')
grid on

subplot(3,2,4)
plot(kepler.Data(:,4)*180/pi)
axis([0 inf 0 1.3*max(kepler.Data(:,4))*180/pi])
xlabel('Time [s]')
ylabel('RAAN [�]')
grid on

subplot(3,2,5)
plot(kepler.Data(:,5)*180/pi)
xlabel('Time [s]')
ylabel('Argument of Perigee [�]')
grid on

subplot(3,2,6)
plot(kepler.Data(:,6)*180/pi)
xlabel('Time [s]')
ylabel('True Anomaly [�]')
grid on
