%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   
%   SEESAT AOCS Simulator
%
%   ss_aocs_actuator_postprocessing.m
%
%   Analyze and plot the actuator bahaviour.
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Reaction wheel dynamics
scrsz = get(groot,'ScreenSize');
fig_react= figure('Name','Reaction Wheel',...
                 'Menubar','none',...
                 'Numbertitle','off',...
                 'Position',[scrsz(1),...
                             scrsz(2),...
                             scrsz(3),...
                             scrsz(4)]);
             
ax_react= axes(fig_react);

% Torque
subplot(3,2,1);
plot(N_c_react.Data(:,1),'r');
hold on
plot(N_react.Data(:,1),'b');
xlabel('Time [s]');
ylabel('Torque x-Axis [Nm]');
legend('Commanded','Real');
%axis([0 inf -5 5])
grid on

subplot(3,2,3);
plot(N_c_react.Data(:,2),'r');
hold on
plot(N_react.Data(:,2),'b');
xlabel('Time [s]');
ylabel('Torque y-Axis [Nm]');
legend('Commanded','Real');
%axis([0 inf -5 5])
grid on

subplot(3,2,5);
plot(N_c_react.Data(:,3),'r');
hold on
plot(N_react.Data(:,3),'b');
xlabel('Time [s]');
ylabel('Torque z-Axis [Nm]');
legend('Commanded','Real');
%axis([0 inf -5 5])
grid on

% Angular velocity
subplot(3,2,2);
plot(ang_velocity_react.Data(:,1)*180/pi,'r');
xlabel('Time [s]');
ylabel('Angular Velocity [°/s]');
%axis([0 inf -5 5])
grid on

subplot(3,2,4);
plot(ang_velocity_react.Data(:,2)*180/pi,'r');
xlabel('Time [s]');
ylabel('Angular Velocity [°/s]');
%axis([0 inf -5 5])
grid on

subplot(3,2,6);
plot(ang_velocity_react.Data(:,3)*180/pi,'r');
xlabel('Time [s]');
ylabel('Angular Velocity [°/s]');
%axis([0 inf -5 5])
grid on


%% Magnetorquer dynamics
fig_magtorquer= figure('Name','Magnetorquer',...
                 'Menubar','none',...
                 'Numbertitle','off',...
                 'Position',[scrsz(1),...
                             scrsz(2),...
                             scrsz(3),...
                             scrsz(4)]);
             
ax_magtorquer= axes(fig_magtorquer);

subplot(3,2,1);
%plot(N_c_mag.Data(:,1),'r');
hold on
plot(N_mag.Data(:,1),'b');
xlabel('Time [s]');
ylabel('Torque x-Axis [Nm]');
legend('Commanded','Real');
grid on

subplot(3,2,3);
%plot(N_c_mag.Data(:,2),'r');
%hold on
plot(N_mag.Data(:,2),'b');
xlabel('Time [s]');
ylabel('Torque y-Axis [Nm]');
legend('Commanded','Real');
grid on

subplot(3,2,5);
%plot(N_c_mag.Data(:,3),'r');
%hold on
plot(N_mag.Data(:,3),'b');
xlabel('Time [s]');
ylabel('Torque z-Axis [Nm]');
legend('Commanded','Real');
grid on

% Current through magnetorquer coils
subplot(3,2,2);
plot(i_c_mag.Data(:,1),'r');
hold on
plot(i_mag.Data(:,1),'b');
xlabel('Time [s]');
ylabel('Magnetorquer Current (Coil x) [A]');
legend('Commanded','Real');
axis([0 inf -1 1])
grid on

subplot(3,2,4);
plot(i_c_mag.Data(:,2),'r');
hold on
plot(i_mag.Data(:,2),'b');
xlabel('Time [s]');
ylabel('Magnetorquer Current (Coil y) [A]');
legend('Commanded','Real');
axis([0 inf -1 1])
grid on

subplot(3,2,6);
plot(i_c_mag.Data(:,3),'r');
hold on
plot(i_mag.Data(:,3),'b');
xlabel('Time [s]');
ylabel('Magnetorquer Current (Coil z) [A]');
legend('Commanded','Real');
axis([0 inf -1 1])
grid on

