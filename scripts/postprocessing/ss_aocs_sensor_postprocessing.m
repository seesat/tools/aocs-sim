%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   
%   SEESAT AOCS Simulator
%
%   ss_aocs_sensor_postprocessing.m
%
%   Analyze and plot the sensor outputs.
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

scrsz = get(groot,'ScreenSize');

%% Visualize gyro measurements
fig_gyro= figure('Name','Gyro Measurments',...
                 'Menubar','none',...
                 'Numbertitle','off');
             
ax_gyro= axes(fig_gyro);


subplot(3,1,1);
plot(ang_velocity_meas.Data(:,1)*180/pi,'r');
hold on
plot(ang_velocity.Data(:,1)*180/pi,'b');
xlabel('Time [s]');
ylabel('Roll rate [°]');
legend('Measured','Real');
axis([0 inf -5 5])
grid on

subplot(3,1,2);
plot(ang_velocity_meas.Data(:,2)*180/pi,'r');
hold on
plot(ang_velocity.Data(:,2)*180/pi,'b');
xlabel('Time [s]');
ylabel('Pitch rate [°]');
legend('Measured','Real');
axis([0 inf -5 5])
grid on

subplot(3,1,3);
plot(ang_velocity_meas.Data(:,3)*180/pi,'r');
hold on
plot(ang_velocity.Data(:,3)*180/pi,'b');
xlabel('Time [s]');
ylabel('Yaw rate [°]');
legend('Measured','Real');
axis([0 inf -5 5])
grid on

%% Visualize magnetometer measurements
fig_mag= figure('Name','Magnetometer Measurments',...
                 'Menubar','none',...
                 'Numbertitle','off');
             
ax_mag= axes(fig_mag);

subplot(3,1,1);
plot(mag_vec_meas.Data(:,1),'r');
hold on
plot(mag_vec.Data(:,1),'b');
xlabel('Time [s]')
ylabel('Magnetic Flux Density [nT]')
legend('Measured','WMM15')
grid on

subplot(3,1,2);
plot(mag_vec_meas.Data(:,2),'r');
hold on
plot(mag_vec.Data(:,2),'b');
xlabel('Time [s]');
ylabel('Magnetic Flux Density [nT]');
legend('Measured','WMM15');
grid on

subplot(3,1,3);
plot(mag_vec_meas.Data(:,3),'r');
hold on
plot(mag_vec.Data(:,3),'b');
xlabel('Time [s]');
ylabel('Magnetic Flux Density [nT]');
legend('Measured','WMM15');
grid on
autoArrangeFigures(1,2)

%% Visualize Sun sensor measurements
fig_sun= figure('Name','Sun sensor Measurments',...
                 'Menubar','none',...
                 'Numbertitle','off',...
                 'Position',[scrsz(1),...
                             scrsz(2),...
                             scrsz(3),...
                             scrsz(4)]);
             
ax_sun= axes(fig_sun);


% Get the measurements for better readability
ss_angles= sun_angles_meas.Data(:,:);


subplot(3,2,1);
plot(ss_angles(:,1)*180/pi,'r');
xlabel('Time [s]');
ylabel('n_x angle [°]');
axis([0 inf 0 90])
grid on

subplot(3,2,2);
plot(ss_angles(:,2)*180/pi,'r');
xlabel('Time [s]');
ylabel('-n_x angle [°]');
axis([0 inf 0 90])
grid on

subplot(3,2,3);
plot(ss_angles(:,3)*180/pi,'r');
xlabel('Time [s]');
ylabel('n_y angle [°]');
axis([0 inf 0 90])
grid on

subplot(3,2,4);
plot(ss_angles(:,4)*180/pi,'r');
xlabel('Time [s]');
ylabel('-n_y angle [°]');
axis([0 inf 0 90])
grid on

subplot(3,2,5);
plot(ss_angles(:,5)*180/pi,'r');
xlabel('Time [s]');
ylabel('n_z angle [°]');
axis([0 inf 0 90])
grid on

subplot(3,2,6);
plot(ss_angles(:,6)*180/pi,'r');
xlabel('Time [s]');
ylabel('-n_z angle [°]');
axis([0 inf 0 90])
grid on


