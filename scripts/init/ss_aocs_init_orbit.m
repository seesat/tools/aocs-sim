%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   
%   SEESAT AOCS Simulator
%
%   ss_aocs_init_orbit.m
%
%   Set the initial kepler elements for the propagated orbit
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SSO %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Semi-major axis [m]
% a0                = R_earth + 500*1000;
% % Eccentricity [-]
% ecc0              = 0.03;
% % Inclination [rad]
% inc0              = acos(-(a0/12352000)^(7/2));
% % Right ascention of the ascending node [rad]
% raan0           = 0;
% % Argument of perigee [rad]
% arg_perigee0    = 90*pi/180;
% % Mean anomaly [rad] (not used as initial condition)
% mean_anom0      = 0;
% % Eccentric anomaly [rad] (not used as initial condition)
% ecc_anom0       = 0;
% % True anomaly [rad]
% true_anom0      = 0;




%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LEO %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Semi-major axis [m]
a0              = 6788 *1000;
% Eccentricity [-]
ecc0            = 0.001;
% Inclination [rad]
inc0            = 51.6*pi/180;
% Right ascention of the ascending node [rad]
raan0           = 0; 
% Argument of perigee [rad]
arg_perigee0    = 0; 
% Mean anomaly [rad] (not used as initial condition)
mean_anom0      = 0;
% Eccentric anomaly [rad] (not used as initial condition)
ecc_anom0       = 0;
% True anomaly [rad]
true_anom0      = 0;
