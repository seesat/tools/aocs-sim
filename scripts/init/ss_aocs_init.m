%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   
%   SEESAT AOCS Simulator
%
%   ss_aocs_init_sim.m
%
%   Set up constants and initial conditions
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Simulation
% Simulation Time [s]
tSim        =180*60;
% Simulation Stepsize [s]
simstepsize =2;

%% Load Bus definitions
load('bus_definition.mat')

%% Astronomical
% general gravity constant [m³/(s²*kg)]
G        = 6.67259e-11;          
% gravity constant of earth [m³/s²]
GM_earth = 3.986004418e14;   
% gravity constant of the sun [m³/s²]
GM_sun   = 1.32712440018e20;  
% gravity constant of the moon [m³/s²]
GM_moon  = 4.902801e12;  

% mean earth radius [m]
R_earth  = 6371008.7714;                   
% sun radius [m]
R_sun    = 6.96e8; 
% moon radius [m]         
R_moon   = 1.738e6;      


% Vector to the sun's center of gravity in the inertial frame on 01.06.2011 [m]
r_sun_I = [5.1422233696e+10, 1.30927698909e+11, 5.6758787969e+10];
% Vector to the moon's center of gravity in the inertial frame on 01.06.2011 [m]
r_moon_I = [1.96824658e+08, 3.08103721e+08, 1.49930567e+08];

% astronomical unit  [m]     
AU       = 1.49597870691e11;       
% Earth's inertial rate unit at J2000 [rad/s]
ERU= 7.2921151467e-5;      

% ENABLE Flags for orbit perturbation models
ENABLE_athmo_drag = 1;
ENABLE_solar_pressure = 1;
ENABLE_flattness= 1;
ENABLE_sun_moon= 1;

%% Environmental constants
% Flattening of earth under its gravitational potential 
J2      = 1082.63*10^-6;   
J3      = -2.53*10^-6;
J4      = -1.61*10^-6;
J5      = -0.13*10^-6;

% Solar Radiation Pressure [N/m²]
p_sun   = 4.56e-6; 

% Athmospheric density [kg/m³]                                      
rho_athm = 1e-12 ;                                                  



%% Initial Conditions

% Initial attitude quaternion
quat0   = [0 0 0 1];
% Initial angular velocity 
ang_velocity0   = [0 0 0]*pi/180;

% Set the initial kepler elements
ss_aocs_init_orbit

%% Sensor Parameters
ss_aocs_sensor_parameters

%% Actuator Parameters
ss_aocs_actuator_parameters

%% Spacecraft Parameters

% Orthogonal surface unit vectors (solar panels)
surface_unit_vector1= [1 0 0];
surface_unit_vector2= [-1 0 0];
surface_unit_vector3= [0 1 0];
surface_unit_vector4= [0 -1 0];
surface_unit_vector5= [0 0 1];
surface_unit_vector6= [0 0 -1];


% Spacecraft inertia matrix in [kg*m²] 
J_sc = [0.0067 0 0; 
        0 0.0333 0;
        0 0 0.333];
                     
 % mass of satellite [kg]                                           
m_sc = 3;

% Spacecraft surface area [m^2]
A_sc= 0.03;

% drag coefficient [-]                                            
drag_coeff = 2.2;

% Reflexion coefficient [-]
reflex_coeff = 0.3;


% balistical coefficient [kg/m²]
ballist_coeff = m_sc/(drag_coeff*A_sc);   

                                                                     
