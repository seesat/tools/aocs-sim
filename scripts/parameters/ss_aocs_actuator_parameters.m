%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   
%   SEESAT AOCS Simulator
%
%   ss_aocs_actuator_parameters.m
%
%   Set up actuator parameter
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Reaction Wheel

% Reaction wheel inertia Moment [kg*m²]
J_react= 5;

% Starting torque [Nm]
RW_starting_torque= 0.005;
% Coulomb friction [Nm]
RW_coulomb_friction= 0.001;
% Stribeck angular velocity [rad/s]
RW_stribeck_speed= 7*2*pi;
% Dynamic friction coeffient [Nm/(rad/s)]
RW_visc_friction= 0.01;
% Maximum reaction wheel angular velocity [rad/s]
ang_velocity_react_max= 2000*2*pi;


%% Magnetorquer
% Magnetorquer solenoids radius [m]
r_mag= 0.01*[1 1 1];

% Count of coil turns in the magnetorquer solenoids [-]
coil_turns= [500 500 500];
% Surface vector of all three magnetorquer solenoids [m^2]
coil_surface_1= pi*r_mag(1)^2*[1 0 0];
coil_surface_2= pi*r_mag(2)^2*[0 1 0];
coil_surface_3= pi*r_mag(3)^2*[0 0 1];
coil_surface= [coil_surface_1, coil_surface_2, coil_surface_3];

% Coil inductance [H]
coil_inductance=  1000*10^-3;
% Coil resistance [Ohm]
coil_resistance= 10;
% Relative permeability of coil [-]
rel_perm_mag= 5000*[1 1 1];

