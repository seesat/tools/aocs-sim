%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   
%   SEESAT AOCS Simulator
%
%   ss_aocs_sensor_parameters.m
%
%   Set up parameters used in the sensor models
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Gyro Parameters

% Gyro Bias [rad/s]
bias_gyro   = 0.012*pi/180;
% Rate Noise density
RND_gyro    = 0.037*pi/180;
% Sample Period Gyro
T_gyro      = simstepsize;
% LPF Cut-off frequency
freq_lpf_gyro   = 12.5*2*pi;
% LPF Damping
damp_lpf_gyro   = 5*sqrt(2)/2;


%% Magnetometer

% Magnetometer Bias [nT]
bias_mag    = 100;
% Rate Noise density
RND_mag     = 100;
% Sample Period Magnetometer
T_mag       = simstepsize;

%% Sun Sensor

% Rate noise density
RND_sunsensor    = 2*pi/180;
