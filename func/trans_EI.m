%   Function name: trans_EI()
%   
%   Description:
%   Transformation from inertia system (J) to earth-fixed system (E)
%   
%   Input arguments:    theta      [rad] or [°] Rotation angle of earth
%                       typ        [-] Type of anlge (Degree/radian),
%                                       default: radian
%
%   Output arguments:   trans_EI   [-] transformation matrix


function [ trans_EI ] = trans_EI( theta, typ )
%trans_OJ() Transformation from inertia system (I) to orbital system (O)
if nargin < 2
    typ= 'radian';
end

if(strcmp(typ,'degree'))
    theta= theta*pi/180;
elseif(~strcmp(typ,'radian'))
    error 'Unknown option for "type"'
end

trans_EI= rotZ(theta);

end
