function [ quat ] = ss_dcm2quat( dcm )
%ss_dcm2quat Own implementation of the conversion from DCM to quaternion

q4 = 0.5 * sqrt(1 + dcm(1,1) + dcm(2,2) + dcm(3,3));

if abs(q4) < 0.1
    
    q1= 0.5 * sqrt(1 + dcm(1,1) - dcm(2,2) - dcm(3,3));
    q2= 1/(4*q1) * (dcm(1,2) + dcm(2,1));
    q3= 1/(4*q1) * (dcm(1,3) + dcm(3,1));
    q4= 1/(4*q1) * (dcm(3,2) - dcm(2,3));
else
    q1= 1/(4*q4) * (dcm(3,2) - dcm(2,3));
    q2= 1/(4*q4) * (dcm(1,3) - dcm(3,1));
    q3= 1/(4*q4) * (dcm(2,1) - dcm(1,2));
end

quat = [q1, q2, q3, q4];
    
end

