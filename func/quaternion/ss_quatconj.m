function [ q_t ] = ss_quatconj( q )
%SS_QUATCONJ Conjugate quaternion

q_t= [-q(1),-q(2),-q(3),q(4)];

end

