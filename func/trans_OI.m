%   Function name: trans_OI()
%   
%   Description:
%   Transformation from inertia system (I) to orbital system (O)
%   
%   Input arguments:    raan            [rad] or [°] right ascension
%                       inc             [rad] or [°] dip
%                       arg_perigee     [rad] or [°] argument of perigee
%                       typ             [-] Type of anlge (Degree/radian),
%                                       default: radian
%
%   Output arguments:   trans_OJ        [-] transformation matrix


function [ trans_OI ] = trans_OI( raan, inc, arg_perigee, typ )
%trans_OJ() Transformation from inertia system (I) to orbital system (O)
if nargin < 4
    typ= 'radian';
end

if(strcmp(typ,'degree'))
    raan= raan*pi/180;
    inc=inc*pi/180;
    arg_perigee=arg_perigee*pi/180;
elseif(~strcmp(typ,'radian'))
    error 'Unknown option for "type"'
end

trans_OI= rotZ(arg_perigee)*rotX(inc)*rotZ(raan);

end
