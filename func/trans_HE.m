%   Function name: trans_HE()
%   
%   Description:
%   Transformation from earth-fixed system (E) to horizontal system (H)
%   
%   Input arguments:    lambda        [rad] or [°] latitude
%                       beta          [rad] or [°] longitude
%                       
%                       typ        [-] Type of anlge (Degree/radian),
%                                       default: radian
%
%   Output arguments:   trans_OJ        [-] transformation matrix

function [ T_HE ] = trans_HE( lambda, beta, typ )
%trans_HE() Transformation from earth-fixed system (E) to horizontal system (H)
if nargin < 3
    typ= 'radian';
end

if(strcmp(typ,'degree'))
    lambda= lambda*pi/180;
    beta=beta*pi/180;
elseif(~strcmp(typ,'radian'))
    error 'Unknown option for "type"'
end

T_HE= rotY(pi/2-beta)*rotZ(lambda);




end

