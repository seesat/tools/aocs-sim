%   Function name: cart2sphere()
%   
%   Description:
%   Transform a vector r from cartesian coordinate system to a spherical
%   coordinate system
%   
%   Input arguments:    r               [] Vector in cartesian coordinates
%
%   Output arguments:   r           [] Radius
%                       azimuth       [rad] Angle in respect to the x axis
%                       elevation         [rad] Angle in respect to the z axis


function [ r, lambda, psi ] = cart2spherical( r )
%Transform a vector r from cartesian coordinate system to a spherical
%coordinate system

x= r(1);
y= r(2);
z= r(3);

lambda = atan2(y,x);
r= sqrt(x*x+y*y+z*z);
psi= acos(z/r);

end
